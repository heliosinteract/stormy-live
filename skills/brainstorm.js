module.exports = function(controller) {
  
  var Confluence = require("confluence-api");
  var request = require("request");
  var config = {
    username: process.env.confluenceUsername,
    password: process.env.confluencePassword,
    baseUrl:  "https://heliosinteract.atlassian.net/wiki",
    version: 5 // Confluence major version, optional 
  }
  
  var cheerio = require('cheerio');
  
  var confluence = new Confluence(config);
  
  // IDs for Labels used in Confluence
  const labelConcept = '8057434';
  const labelBrainstorm = '8057416';
  
  function branchFromLink(bot, message, link){

    let pageid = getIdFromLink(link);
    let endpoint = config.baseUrl + "/rest/api/content/";
    let query = pageid + "?expand=ancestors,body.view,space";

    request.get(endpoint + query, function(err, response, body){
      
      if(err){
        
        bot.reply(message, "Sorry, couldn't find that Confluence page");
        
      } else {
        var page = JSON.parse(body);

        if (typeof page.id === 'undefined'){
          bot.reply(message, "Sorry, couldn't find that Confluence page");
          return;
        }

        let ancestors = page.ancestors;

        // figure out which sort of page we're looking at via the ancestors
        if(ancestors.length > 0){

          let ancestorIds = [];
          ancestors.forEach(function(ancestor){
            ancestorIds.push(ancestor.id);
          });

          if(ancestorIds.includes(labelBrainstorm)){

            startMultiBrainstorm(bot, message, page);

          } else if(ancestorIds.includes(labelConcept)){

            startBrainstorm(bot, message, page)

          } else {
            // Unknown type
            startFreeformBrainstorm(bot, message, page);

          }

        } else {
          // No labels found
          startFreeformBrainstorm(bot, message, page);
        }
      }
      
    }).auth(config.username, config.password, true);
    
  }
  
  function startMultiBrainstorm(bot, message, page){
    console.log("startMultiBrainstorm");
    
    let html = page.body.view.value;
    const $ = cheerio.load(html);
    let table = $('table').get(0);
    let rows = $(table).find('tr').toArray();
    rows.shift();
    
    rows.forEach(function(row){
      let href = $(row.children[0]).find('a').attr('href');
      //TODO next: figure out if we've got an ID or SPACE&Title link, then kick off a single brainstorm with that link
    });
  }
  
  function startBrainstorm(bot, message, page){
    
    // Get Info rows from page
    let html = page.body.view.value;
    const $ = cheerio.load(html);
    let table = $('table').get(0);
    let rows = $(table).find('tr').toArray();
    let info = [];
    rows.shift();
    
    rows.forEach(function(row){
      let key = $(row.children[0]).text();
      let value = $(row.children[1]).text();
      
      if(key.length > 0 && value.length > 0){
        info.push({key:key, value:value});
      }
    });

    // Construct fields from Confluence page table entries
    let fields = [];
    info.forEach(function(line){
        fields.push({
            "title": line.key,
            "value": line.value,
            "short": true
        });
    });

    let reply = {
        attachments:[
            {
                title: page.title,
                title_link:page._links.base + page._links.tinyui,
                callback_id:'stormy-callback',
                attachment_type:'default',
                color:'good',
                actions:[
                    {
                        "name":"start-timer",
                        "text":"Start",
                        "value":"start-timer",
                        "type":"button"
                    }
                ],
                fields:fields
            }
        ]
    };
    
    controller.storage.channels.save({
          id:message.channel, 
          name:page.title,
          ideas:[],
          startTime:message.ts,
          space:page.space.key,
          title:'Brainstorm Results: ' + page.title + " " + new Date().toLocaleDateString(),
          parentId:page.id,
          active: true
        },
          function(err){
          if(err){
            bot.reply("Couldn't save data because " + err); 
          } else {
            bot.reply(message, reply);
          }
        });
    
  }
  
  function startFreeformBrainstorm(bot, message, page){
    bot.reply(message, "I don't know how to start this kind of brainstorm yet");
  }
  
  // Start new Brainstorm
  controller.hears('start', 'direct_mention', function(bot, message){
    
    // extract link from message text
    var links = message.text.match(/\bhttps?:\/\/\S+/gi);
    if(links.length > 0){
      branchFromLink(bot, message, links[0]);
    } else {
      bot.reply(message, "No Confluence link found. Try again.");  
    }
    
    });

    // receive an interactive message, and reply with a message that will replace the original
    controller.on('interactive_message_callback', function(bot, message) {

        // check message.actions and message.callback_id to see what action to take...
        if(message.callback_id === "stormy-callback"){
          //console.dir(message);
          let oldMessage = message.original_message.attachments[0];
          if(message.text ==="start-timer"){
            let newMessage = {
                attachments:[
                    {
                        title: oldMessage.title,
                        title_link: oldMessage.title_link,
                        callback_id:'stormy-callback',
                        attachment_type:'default',
                        color:'good',
                        actions:[
                            {
                                "name":"finish",
                                "text":"Finish",
                                "value":"finish",
                                "type":"button"
                            }
                        ],
                        fields:oldMessage.fields
                    }
                ]
            };
            bot.replyInteractive(message, newMessage);
          }
          else if(message.text ==="finish"){

              let newMessage = {
                  attachments:[
                      {
                          title: oldMessage.title,
                          title_link: oldMessage.title_link,
                          callback_id:'stormy-callback',
                          attachment_type:'default',
                          color:'good',
                          fields:oldMessage.fields
                      }
                  ]
              };

              bot.replyInteractive(message, newMessage);
              finishBrainstorm(bot, message, true);
          }
        }

    });
  
    // Update Ideas as they come in
   controller.hears('(.*)',['ambient'],function(bot,message) {
     controller.storage.channels.get(message.channel, function(err, channel_data){
      if(err || typeof(channel_data) === 'undefined'){
        console.log("Got error in tracking ambient message: " + err);
      } else {
        if(channel_data.active === true){
          channel_data.ideas.push(message);
          controller.storage.channels.save(channel_data);
        }
      }
    });
     
  });

   function finishBrainstorm(bot, message, fromButton){
       fromButton = fromButton || false;
       controller.storage.channels.get(message.channel, function(err, channel_data){
           if(err){
               console.log("Got error in tracking finish message: " + err);
           } else {
               if(channel_data.active === true){

                   bot.api.channels.history({token:bot.config.bot.app_token, channel:message.channel, oldest:channel_data.startTime, inclusive:false}, function(err, response){
                       if(err){
                           bot.reply(message, endBorder + '\n' + "Error retrieving the message history from the channel. \n" + endBorder);
                       } else {
                           // remove first and optionally last messages because they can be the 'start' and 'end' commands. Skip the last one if this was triggered from a button
                           let offset = fromButton ? 0 : 1;
                           var ideas = response.messages.slice(offset, response.messages.length - 1);
                           ideas.reverse();
                           let body = bodyFromIdeas(ideas, bot.config.bot.user_id);

                           confluence.postContent(channel_data.space, channel_data.title, body, channel_data.parentId, function(err, postResult){
                               if(err){
                                   console.log('error: in postContent ' + err);
                                   bot.reply(message, endBorder + '\n' + "Error saving the brainstorm to Confluence: " + err + "\n" + endBorder);
                               } else {
                                   let savedPage = postResult._links.base + postResult._links.tinyui;
                                   let reply = {
                                       attachments: [
                                           {
                                               "color":"good",
                                               "text": channel_data.ideas.length + ' ideas saved: \n' + savedPage
                                           }
                                       ]
                                   };
                                   bot.reply(message, reply);


                               }
                           });

                       }
                   })

               }
           }
       });
   }
  
  // Finish Brainstorm
  controller.hears('finish', 'direct_mention', finishBrainstorm);
  
  let endBorder = "*-----*\n";
  function bodyFromIdeas(ideas, botUserId){
    
    // generate ideaList string
    let ideaList = "<ul>";
    
    ideas.forEach(function (idea){
      if(idea.user !== botUserId){
        // convert slack links into html links
        let converted = idea.text.replace(/<(https?.+?)>/, "<a href=\"$1\">$1</a>");
          // strip all other tag types for now
          var cleanText = converted.replace(/<(?!\/?a(?=>|\s.*>))\/?.*?>/g, "");
          ideaList += "<li>" + cleanText + "</li>"
      }
    });
    ideaList += "</ul>";

    return ideaList;
  }
  
  function getSpaceAndTitleFromLink(link){
      var parts = link.split('/');
      var space = parts[5];
      var title = parts[8].replace(/\+/g, ' ');
      var parent = parts[7];
      title = title.replace('>', '');
      return {space: space, title:title, parent:parent};
  }

  function getIdFromLink(link){
    let parts = link.split('/');
    let id = parts[7];
    return id;
  }
  
  function keyFromStartCommand(command){
    var name = command.replace("start ", "");
    name = name.replace(/[^0-9a-zA-Z-_ ]/g, '');
    return name;
  }
  
  // parseUri 1.2.2
// (c) Steven Levithan <stevenlevithan.com>
// MIT License

function parseUri (str) {
	var	o   = parseUri.options,
		m   = o.parser[o.strictMode ? "strict" : "loose"].exec(str),
		uri = {},
		i   = 14;

	while (i--) uri[o.key[i]] = m[i] || "";

	uri[o.q.name] = {};
	uri[o.key[12]].replace(o.q.parser, function ($0, $1, $2) {
		if ($1) uri[o.q.name][$1] = $2;
	});

	return uri;
};

parseUri.options = {
	strictMode: false,
	key: ["source","protocol","authority","userInfo","user","password","host","port","relative","path","directory","file","query","anchor"],
	q:   {
		name:   "queryKey",
		parser: /(?:^|&)([^&=]*)=?([^&]*)/g
	},
	parser: {
		strict: /^(?:([^:\/?#]+):)?(?:\/\/((?:(([^:@]*)(?::([^:@]*))?)?@)?([^:\/?#]*)(?::(\d*))?))?((((?:[^?#\/]*\/)*)([^?#]*))(?:\?([^#]*))?(?:#(.*))?)/,
		loose:  /^(?:(?![^:@]+:[^:@\/]*@)([^:\/?#.]+):)?(?:\/\/)?((?:(([^:@]*)(?::([^:@]*))?)?@)?([^:\/?#]*)(?::(\d*))?)(((\/(?:[^?#](?![^?#\/]*\.[^?#\/.]+(?:[?#]|$)))*\/?)?([^?#\/]*))(?:\?([^#]*))?(?:#(.*))?)/
	}
};

};